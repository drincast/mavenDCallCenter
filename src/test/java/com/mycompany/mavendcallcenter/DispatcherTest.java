/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mavendcallcenter;

import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Desarrollo
 */
public class DispatcherTest {
    
    public DispatcherTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    
    
    
    @Test
    public void testDispatchCon10CallCompacto(){
        System.out.println("Ejecutar 10 Llamadas con 10 empleados de tipo aleatorio");
        int faltaPorAtender = 0;
        List<Llamada> lstLlamadas = new ArrayList<Llamada>();
        Dispatcher objDispatcher = new Dispatcher();
        
        for (int i = 0; i < 10; i++) {
            lstLlamadas.add(new Llamada((short)0));
        }
        objDispatcher.RegistrarEmpleados(10);
        
        for(Llamada objLlamada : lstLlamadas){                    
            objDispatcher.IniciarAtencion(objLlamada);
        }
        
        boolean finalizaronLosHilos = false;
        
        while(finalizaronLosHilos == false){
            for(Thread objT : objDispatcher.lstThreads){                    
                if(objT.isAlive()){
                    finalizaronLosHilos = false;
                    break;
                }
                else{
                    finalizaronLosHilos = true;
                }
            }
        }
        
        for(Llamada objLlamada : lstLlamadas){                    
            if(objLlamada.estado == 0 || objLlamada.estado == 1){
                faltaPorAtender++;
            }
        }
        if(faltaPorAtender > 0)
        {
            fail("Quedaron Llamadas sin atender");
        }
    }
    
    @Test
    public void testDispatchCon20CallCompacto(){
        System.out.println("Ejecutar 20 Llamadas con 10 empleados de tipo aleatorio");
        int faltaPorAtender = 0;
        List<Llamada> lstLlamadas = new ArrayList<Llamada>();
        Dispatcher objDispatcher = new Dispatcher();
        
        if(objDispatcher.lstThreads != null){
            if(objDispatcher.lstThreads.size() > 0){
                objDispatcher.lstThreads.clear();
            }
        }
        
        for (int i = 0; i < 20; i++) {
            lstLlamadas.add(new Llamada((short)0));
        }
        objDispatcher.RegistrarEmpleados(10);
        
        for(Llamada objLlamada : lstLlamadas){                    
            objDispatcher.IniciarAtencion(objLlamada);
        }
        
        objDispatcher.AtenderLlamasEnEspera();
        
        boolean finalizaronLosHilos = false;
        
        while(finalizaronLosHilos == false){
            for(Thread objT : objDispatcher.lstThreads){
                if(objT != null){
                    if(objT.isAlive()){
                        finalizaronLosHilos = false;
                        break;
                    }
                    else{
                        finalizaronLosHilos = true;
                    }
                }
            }
        }
        
        faltaPorAtender = 0;
        for(Llamada objLlamada : lstLlamadas){                    
            if(objLlamada.estado == 0 || objLlamada.estado == 1){
                faltaPorAtender++;
                //System.out.println("Llamada: " + objLlamada.getId() + " - estado: " + objLlamada.estado);
            }
        }
        if(faltaPorAtender > 0)
        {
            fail("Quedaron Llamadas sin atender");
        }
    }

    /**
     * Test of dispatchCall method, of class Dispatcher.
     */
//    @Test
//    public void testDispatchCall() {
//        System.out.println("dispatchCall");
//        Llamada objLlNueva = null;
//        Empleado objEmpleado = null;
//        Dispatcher instance = new Dispatcher();
//        instance.dispatchCall(objLlNueva, objEmpleado);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
    
}
