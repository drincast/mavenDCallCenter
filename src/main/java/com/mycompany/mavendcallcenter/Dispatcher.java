/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mavendcallcenter;

import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

/**
 * Controla la atención de las llamadas y la asignación de las llamadas a los empleados
 * @author Rubén Orozco
 */
public class Dispatcher {
    private List<Operador> lstOperadores;
    private List<Supervisor> lstSupervisores;
    private List<Director> lstDirectores;
    private List<Llamada> lstLlamadasEspera;
    public List<Thread> lstThreads;
    
    public Dispatcher() {
        lstLlamadasEspera = new ArrayList<Llamada>();
        lstOperadores = new ArrayList<Operador>();
        lstSupervisores = new ArrayList<Supervisor>();
        lstDirectores = new ArrayList<Director>();
        lstThreads = new ArrayList<Thread>();
    }
    
    public String NumeroLlamadasEnEspera(){
        String res = "0";
        if(this.lstLlamadasEspera != null){
            res = Integer.toString(this.lstLlamadasEspera.size());
        }        
        return res;
    }
    
    public Empleado BuscarEmpleadoDisponible(){
        Empleado objEmpleado = null;
        
        for(Empleado obj : lstOperadores){
            if(obj.getbOcupado() == false){
                objEmpleado = obj;
                break;
            }
        }
        
        if(objEmpleado != null){
            for(Empleado obj : lstSupervisores){
                if(obj.getbOcupado() == false){
                    objEmpleado = obj;
                    break;
                }
            }
            
            if(objEmpleado != null){
                for(Empleado obj : lstDirectores){
                    if(obj.getbOcupado() == false){
                        objEmpleado = obj;
                        break;
                    }
                }
            }
        }
        
        return objEmpleado;
    }
    
    /**
     * registra los empleados que atenderan, el tipo del empleado se asigna de forma aleatoria
     * @param numEmpleados 
     */
    public void RegistrarEmpleados(int numEmpleados){
        Random rnd = new Random();
        int tipo = 0;
        
        for (int i = 0; i < numEmpleados; i++) {
            tipo = (int)(rnd.nextDouble() * 3);
            switch(tipo){
                case ParametrosApp.EMPLEADO_OPERADOR:
                    lstOperadores.add(new Operador(false, 0));
                    break;
                case ParametrosApp.EMPLEADO_SUPERVISOR:
                    lstSupervisores.add(new Supervisor(false, 0));
                    break;
                case ParametrosApp.EMPLEADO_DIRECTOR:
                    lstDirectores.add(new Director(false, 0));                    
                    break;
                default:
                    lstOperadores.add(new Operador(false, 0));
            }
        }
    }
    
    /**
     * Retorna un string con la descripción de los empleados registrados para atender llamadas
     * @return 
     */
    public String DescripEmpleadosRegistrados(){
        StringBuilder strRes = new StringBuilder();
        
        strRes.append("Directores: ");
        for(Empleado objE : lstDirectores){
            strRes.append(objE.idEmpleado).append(", ");
        }
        
        strRes.append("\nSupervisores: ");
        for(Empleado objE : lstSupervisores){
            strRes.append(objE.idEmpleado).append(", ");
        }
        
        strRes.append("\nOperadores: ");
        for(Empleado objE : lstOperadores){
            strRes.append(objE.idEmpleado).append(", ");
        }
        
        return strRes.toString();
    }
    
    /**
     * Inicia la atención de una llamada
     * 
     * Contiene parte de la la solución propuesta de los primeros puntos extra del ejercicio propuesto
     * 
     * @param objLlamada: llamada a ser atentida por un empleado
     */
    public void IniciarAtencion(Llamada objLlamada){
        Empleado objEmpleado = BuscarEmpleadoDisponible();
        
        if(objEmpleado != null){
            dispatchCall(objLlamada, objEmpleado);
        }
        else{
            lstLlamadasEspera.add(objLlamada);
        }
    }
    
    /**
     * Propuesta de solición cuando hay llamadas sin atender
     * Inicia la atención de las llamadas que estan en la lista de espera, se realiza un ciclo
     * hasta que la lista este sin llamadas
     */
    public void AtenderLlamasEnEspera(){
        Iterator<Llamada> iterador = null; 
        Empleado objEmpleado = null;
        Llamada objTemLlamada = null;
        try {
            while(this.lstLlamadasEspera.size() != 0){
                iterador = this.lstLlamadasEspera.iterator();
                while(iterador.hasNext()){
                    objTemLlamada = iterador.next();
                    objEmpleado = BuscarEmpleadoDisponible();
                    if(objEmpleado != null){
                        if(objTemLlamada != null && objTemLlamada.estado == ParametrosApp.ESTADO_LLAMADA_NOATENDIDA)
                            dispatchCall(objTemLlamada, objEmpleado);
                    }
                    objEmpleado = null;
                    if(objTemLlamada.estado == ParametrosApp.ESTADO_LLAMADA_FINALIZADA){
                        iterador.remove();
                    }
                }
            }
        } catch (Exception e) {
            System.err.println("Error al atender la lista de llamadas en espera: " + e. getMessage());
            Logger.getLogger("LogMain").log(Level.SEVERE, null, e);
        }
    }
    
    /**
     * Se encarga de asignar una llamada a un empleado y de indicarle al emplado que inicie
     * con la atención.
     * 
     * @param objLlNueva, llamada a ser atendida
     * @param objEmpleado , empleado que atendera la llamada
     */
    public void dispatchCall(Llamada objLlNueva, Empleado objEmpleado){
        Random rnd = new Random();
        
        int tiempo = (int)(rnd.nextDouble() * ParametrosApp.MIN_MS_ATENLLAMADA + (ParametrosApp.MAX_MS_ATENLLAMADA / 2));
        //evitar numeros negativos
        if(tiempo < 0)
            tiempo = tiempo * -1;
        
        objEmpleado.OcuparEmpleado(true, tiempo, objLlNueva);
        Thread objT = new Thread(objEmpleado);
        this.lstThreads.add(objT);
        objT.start();
    }
}
