/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mavendcallcenter;

/**
 * Maneja parametros constantes, que sirven como configuración para la aplicación
 * @author Rubén Orozco
 */
public class ParametrosApp {
    static final int MIN_MS_ATENLLAMADA = 5000; /*en milisegundos*/
    static final int MAX_MS_ATENLLAMADA = 10000; /*en milisegundos*/
    static final int NUM_LLAMADAS_POR_DEFECTO = 10;
    static final int NUM_EMPLEADOS_POR_DEFECTO = 10;
    static final int ESTADO_LLAMADA_NOATENDIDA = 0;
    static final int ESTADO_LLAMADA_ENATENCION = 1;
    static final int ESTADO_LLAMADA_FINALIZADA = 2;
    static final int EMPLEADO_OPERADOR = 0;
    static final int EMPLEADO_SUPERVISOR = 1;
    static final int EMPLEADO_DIRECTOR = 2;
}
