/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mavendcallcenter;

/**
 * Clase que Hereda de Empleado, representa aun empleado Director
 * @author Rubén Orozco
 */
public class Director extends Empleado{
    public Director(boolean bOcupado, int vTime) {
        super(bOcupado, vTime);
        CrearIdEmpleado("D");
    }
}