/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mavendcallcenter;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Clase para representar a un empleado general
 * 
 * @author Rubén Orozco
 */
public class Empleado implements Runnable{
    static int identidad = 0;
    protected String idEmpleado;
    private boolean bOcupado;   /*indica si esta ocupado*/
    private int intTiempo;      /*tiempo de atención de la llamada actual en milisegundos*/
    private Llamada objLlamada; /*llamada que se esta atendiendo*/

    public Empleado(boolean bOcupado, int vTime) {        
        this.bOcupado = bOcupado;
        this.intTiempo = vTime;
        
        if(this.identidad >= 1)
            this.identidad++;
        else
            this.identidad = 1;
    }
    
    public boolean getbOcupado() {
        return bOcupado;
    }

    public void setbOcupado(boolean bOcupado) {
        this.bOcupado = bOcupado;
    }

    public void setIntTiempo(int intTiempo) {
        this.intTiempo = intTiempo;
    }    
    
    /**
     * Método para asignar un ID a los empleados
     * @param pre 
     */
    public void CrearIdEmpleado(String pre){
        this.idEmpleado = pre + Integer.toString(identidad);
    }
    
    /**
     * Método para iniciar los atributos de un empleado cuando esta ocupado, atendiendo una llamada
     * 
     * @param bOcupado
     * @param vTime
     * @param objLlamada 
     */
    public void OcuparEmpleado(boolean bOcupado, int vTime, Llamada objLlamada){
        this.bOcupado = bOcupado;
        this.intTiempo = vTime;
        this.objLlamada = objLlamada;
        this.objLlamada.estado = ParametrosApp.ESTADO_LLAMADA_ENATENCION;
    }
    
    /**
     * implementación del método run, heredado de la clase Runnable, al finalizar se establecen los atributos
     * del empleado a su estado libre.
     */
    @Override
    public void run() {
        System.out.println("Inicio atencion de Llamada " + this.objLlamada.getId() + " - Empleado: " + this.idEmpleado 
                + " - tiempo atención: " + this.intTiempo + " - ocupado " + this.bOcupado);
        try {
            Thread.sleep(this.intTiempo);
            
            System.out.println("Fin atencion de Llamada " + this.objLlamada.getId() + " - tiempo atención: " + this.intTiempo 
                    + " - Empleado: " + this.idEmpleado);
            this.objLlamada.estado = ParametrosApp.ESTADO_LLAMADA_FINALIZADA;
            this.intTiempo = 0;
            this.bOcupado = false;
        } catch (InterruptedException ex) { // Sleep puede lanzar una excepción que aborte la ejecución del hilo.
            System.out.println("Error en la atención de la llamada: " + Integer.toString(this.objLlamada.getId()) 
                    + " - Empleado: " + this.idEmpleado);            
            Logger.getLogger(Empleado.class.getName()).log(Level.SEVERE, null, ex);
            return;
        }
    }
}