/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mavendcallcenter;

/**
 * Representa una llamada
 * 
 * @author Rubén Orozco
 */
public class Llamada {
    static int identidad = 0;
    private int id;
    public short estado;
    
    public Llamada(short estado) {
        this.estado = estado;
        
        if(this.identidad >= 1)
            this.identidad++;
        else
            this.identidad = 1;
        
        this.id = this.identidad;
    }
        
    public short getEstado() {
        return estado;
    }

    public int getId() {
        return id;
    }
}