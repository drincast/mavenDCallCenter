/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mavendcallcenter;

import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Rubén Orozco
 * Clase de inicio de ejecución de la aplicación
 */
public class AppCallCenter {

    /**
     * metodo de inicio de la aplicación
     * 
     * @param args argumentos de linea de comandos
     */
    public static void main(String[] args) {
        int numLlamadas = 0;
        int numEmpleados = 0;
        List<Llamada> lstLlamadas = new ArrayList<Llamada>();
        Dispatcher objDispatcher = new Dispatcher();
        
        try {
            if(args.length >= 1){
                try {
                    numLlamadas = Integer.parseInt(args[0]);
                } catch (Exception e) {
                    System.out.println("Error en el tipo de dato del parametro de entrada, debe ser un nuero entero positivo, "
                            + "por defecto se establece el numero de llamadas a 10");
                    numLlamadas = 10;
                }
                
                for (int i = 0; i < numLlamadas; i++) {
                    lstLlamadas.add(new Llamada((short)0));
                }
            }
            else{
                for (int i = 0; i < ParametrosApp.NUM_LLAMADAS_POR_DEFECTO; i++) {
                    lstLlamadas.add(new Llamada((short)0));
                }
            }
            
            if(args.length >= 2){
                try {
                    numEmpleados = Integer.parseInt(args[1]);
                } catch (Exception e) {
                    System.out.println("Error en el tipo de dato del parametro de entrada (número de empleados), debe ser un número entero "
                            + "positivo, por defecto se establece el número de empleados al configurado en la aplicación ParametrosApp.NUM_EMPLEADOS_POR_DEFECTO");
                    numEmpleados = ParametrosApp.NUM_EMPLEADOS_POR_DEFECTO;
                }
                
                objDispatcher.RegistrarEmpleados(numEmpleados);
            } else{
                objDispatcher.RegistrarEmpleados(ParametrosApp.NUM_EMPLEADOS_POR_DEFECTO);
            }
            
            StringBuilder msj = new StringBuilder();
            for(Llamada objLlamada : lstLlamadas){
                    msj.append("Llamada ").append(objLlamada.getId()).append(", ");
            }            
            System.out.println("Entrada de Llamadas: " + msj.toString());            
            msj.delete(0, msj.length() - 1);
            
            System.out.println("Empleados\n" + objDispatcher.DescripEmpleadosRegistrados() + "\n");
            
            for(Llamada objLlamada : lstLlamadas){                    
                    objDispatcher.IniciarAtencion(objLlamada);
            }
            
            if(args.length >= 3){
                if(args[2].equals("1")){
                    objDispatcher.AtenderLlamasEnEspera();
                }
            }
        } catch (Exception e) {
            Logger.getLogger("LogMain").log(Level.SEVERE, null, e);
        }
    }
}
