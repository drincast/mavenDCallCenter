/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mavendcallcenter;

/**
 * Clase que Hereda de Empleado, representa aun empleado Supervisor
 * @author Rubén Orozco
 */
public class Supervisor extends Empleado{
    public Supervisor(boolean bOcupado, int vTime) {
        super(bOcupado, vTime);
        CrearIdEmpleado("S");
    }
}
