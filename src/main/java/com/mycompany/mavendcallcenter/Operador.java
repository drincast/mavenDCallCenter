/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mavendcallcenter;

/**
 * Clase que Hereda de Empleado, representa aun empleado Operador
 * @author Rubén Orozco
 */
public class Operador extends Empleado{
    public Operador(boolean bOcupado, int vTime) {
        super(bOcupado, vTime);
        CrearIdEmpleado("O");
    }
}
