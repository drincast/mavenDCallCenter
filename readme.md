# Ejercicio en Java - Call Center con Concurrencia

## Solución

### Puntos a principales para el desarrollo del ejercicio.

* Call Center con tres tipos de empleados (Director, Supervisor y Operador).

* Atención de Llamadas, primero los Operadores, si se ocupan todos pasan las llamas a los supervisores si se ocupan todos pasan a los directores.

* Diseñar el modelado de clases y diagramas UML necesarios para documentar y comunicar el diseño. 

* Debe existir una clase Dispatcher encargada de manejar las llamadas, y debe contener	el método dispatchCall para que las asigne a los empleados disponibles.
 
* La clase Dispatcher debe tener la capacidad	de poder procesar 10 llamadas al mismo tiempo (de modo concurrente).

* Cada llamada	puede durar un tiempo aleatorio entre 5	y 10 segundos. 

* Debe tener un	test unitario donde lleguen 10	llamadas.

## Modelado

### Diagrama de Clases
![alt text](https://gitlab.com/drincast/mavenDCallCenter/raw/master/documentacion/Class%20Diagram%2001.png)

![alt text](https://gitlab.com/drincast/mavenDCallCenter/raw/master/documentacion/Sequence%20Diagram.png)

## Notas sobre la solución

# Que se uso
* JDK 1.8
* IDE NetBeans 8.2
* GitLab online

## Sobre la aplicación
* Aplicación de consola, para ejecutar, se debe tener instalado maven y configurado en las variables de entono.
* Ejemplo de ejecución: mvn exec:java -Dexec.mainClass="com.mycompany.mavendcallcenter.AppCallCenter" -Dexec.args="20 10 1",
  el anterior ejemplo debe ser ejecuta desde la carpeta del proyecto, los parámetros son, el primer parámetro (20) corresponde
  al número de Llamadas a procesar, el segundo parámetro (10) corresponde al número de empleados que atienden llamadas,
  el tercer parámetro si es 1, indica que debe ejecutar las llamadas que están en espera (para proponer una solución a los
  2 primeros extras).
* Por defecto al ejecutar la aplicación sin parámetros, se ejecuta por defecto con 10 Llamadas y 10 Empleados.

## Puntos extra
* Propuesta de solución para los 2 primeros puntos: cuando entran mas de 10 llamadas y no hay empleado Libre (punto extra 1 y 2) 
  Se toma como solución una lista de llamadas sin atender, la idea es, cada llamada que no puede ser atendida entra a una lista 
  (lstLlamasEnEspera), luego se ejecuta un método que asigna una llamada de la lista a un empleado libre 
  (que ha terminado de atender una llamada), este método se ejecuta con un ciclo controlado por el tamaño de la lista de 
  llamadas en espera. 
  El método se encuentra en la clase Dispatcher linea de código 145, en el método IniciarAtencion de la clase Dispacher se 
  encuentra la lógica para llenar la lista de llamadas en espera. 
* Se agrega una prueba unitaria (testDispatchCon20CallCompacto) que realiza la atención de 20 llamadas con 10 empleados,
  para mostrar la solución propuesta a los 2 primeros puntos extras.